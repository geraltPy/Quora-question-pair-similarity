# Quora_question_pair_similarity

The approach can be divided into the following steps:

1. Preprocessing
1. Featurization
1. Visualization and removing redundant features
1. Vectorization of words
1. Applying machine learning models
1. Deployment

First we split the data into train and test sets with a ratio of 70/30.

Detail Steps below:

### Step 1: Preprocessing

The preprocessing with following steps:

* Convert to lowercase
* Remove punctuations
* Replace some numerical values with strings (Eg: 1,000,000 with 1m)
* Remove HTML tags
* Replace some characters with their string equivalents (Eg: $, % @ etc.)
* Decontract words
* Stop word removal

### Step 2: Featurization

The following features were extracted:

- **Token features**
  1. **q1_len**: Number of characters in question 1
  1. **q2_len**: Number of characters in question 2
  1. **q1_words**: Number of words in question 1
  1. **q2_words**: Number of words in question 2
  1. **words_total**: Sum of **q1_words** and **q2_words**
  1. **words_common**: Number of words which occur in question 1 and two, reapeated occurances are not counted
  1. **words_shared**: Fraction of **words_common** to **words_total**
  1. **cwc_min**: This is the ratio of the number of common words to the length of the smaller question
  1. **cwc_max**: This is the ratio of the number of common words to the length of the larger question
  1. **csc_min**: This is the ratio of the number of common stop words to the smaller stop word count among the two questions
  1. **csc_max**: This is the ratio of the number of common stop words to the larger stop word count among the two questions
  1. **ctc_min**: This is the ratio of the number of common tokens to the smaller token count among the two questions
  1. **ctc_max**: This is the ratio of the number of common tokens to the larger token count among the two questions
  1. **last_word_eq**: 1 if the last word in the two questions is same, 0 otherwise
  1. **first_word_eq**: 1 if the first word in the two questions is same, 0 otherwise
  1. **num_common_adj**: This is the number of common adjectives in question1 and question2
  1. **num_common_prn**: This is the number of common proper nouns in question1 and question2
  1. **num_common_n**: This is the number of nouns (non-proper) common in question1 and question2

- **Fuzzy features**

  1. **fuzz_ratio**: fuzz_ratio score from fuzzywuzzy
  1. **fuzz_partial_ratio**: fuzz_partial_ratio from fuzzywuzzy
  1. **token_sort_ratio**: token_sort_ratio from fuzzywuzzy
  1. **token_set_ratio**: token_set_ratio from fuzzywuzzy



- **Length features**
  1. **mean_len**: Mean of the length of the two questions (number of words)
  1. **abs_len_diff**: Absolute difference between the length of the two questions (number of words)
  1. **longest_substr_ratio**: Ratio of the length of the longest substring among the two questions to the length of the smaller question

### Step 4: Vectorization
Two vectorization approaches were considered:

*   TF - IDF vectors (each question has a dimensionality of 73459)
*   Word Vectors from RoBERT model using sentence transformers

### Step 5: Trainig and Testing machine learning models

There were 3 model approaches.

1. Model 1: Only token features, no word vector features. Very low dimensionality but limited semantic information.
1. Model 2:  Just the TF - IDF word vectors themselves. Extremely high dimensionality,  incorporates a lot of semantic information but no token information.
1. Model 3: Using Word Embeddings from RoBERT model with 768 dimensions

Models experimented are:
1. Logistic regression: Hyperparameter search over alpha and regularization method.
1. Naive Bayes: Hyperparameter search over alpha.
1. GBDT: Hyperparameter search over number of estimators (number of boosting rounds) and max depth of estimators.

